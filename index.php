<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S5 Activity</title>
</head>
<body>
	<?php session_start(); ?>

	<?php if(!isset($_SESSION['emailAddress'])): ?>

		<form method="POST" action="./server.php">
			<input type="hidden" name="action" value="login"/>

			Email Address: <input type="email" name="emailAddress" required/>
			Password <input type="password" name="password" required>

			<button type="submit">Login</button>
		</form>
		
	<?php else: ?>

		<p>Hello, <?= $_SESSION['emailAddress']; ?></p>
		<form method="POST" action="./server.php">
			<input type="hidden" name="action" value="logout">
			<button type="submit">Logout</button>
		</form>

	<?php endif; ?>
</body>
</html>
