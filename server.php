<?php

session_start();

class User{
	public function login($emailAddress, $password){
		if($emailAddress === "johnsmith@gmail.com" && $password === '1234'){
			$_SESSION['emailAddress'] = $emailAddress;
		}
	}

	public function logout(){
		session_destroy();
	}
}

$user = new User();

if($_POST['action'] === 'login'){
	$user->login($_POST['emailAddress'], $_POST['password']);
}else if($_POST['action'] === 'logout'){
	$user->logout();
}

header('Location: ./index.php');
